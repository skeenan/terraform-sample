resource "google_project" "project" {
  name       = "Sandbox"
  project_id = "sk-sandbox-02"
  folder_id  = var.root_folder_id
}
resource "google_project_iam_binding" "project" {
  project = google_project.project.project_id
  role    = "roles/owner"

  members = [
    "user:skeenan@zencore.dev",
  ]
}