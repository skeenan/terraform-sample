variable "project_id" {
  default     = ""
  description = "GCP Project ID"
}
variable "root_folder_id" {
  default     = ""
  description = "GCP Folder ID"
}